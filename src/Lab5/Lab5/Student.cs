﻿using System;

namespace Lab5
{
    [Serializable]
    public class Student
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Patronym { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Date { get; set; }
        public string Index { get; set; }
        public string Faculty { get; set; }
        public string Specialty { get; set; }
        public int Progress { get; set; }

        public Student() { }

        public Student(string name, string lastname, string patronym, DateTime birthday, DateTime date, string index, string faculty, string specialty, int progress)
        {
            this.Name = name;
            this.Lastname = lastname;
            this.Patronym = patronym;
            this.Birthday = birthday;
            this.Date = date;
            this.Index = index;
            this.Faculty = faculty;
            this.Specialty = specialty;
            this.Progress = progress;
        }

        public override string ToString()
        {
            return $"{Name}\t " +
                $"{Lastname}\t " +
                $"{Patronym}\t " +
                $"{Birthday}\t " +
                $"{Date}\t " +
                $"{Index}\t " +
                $"{Faculty}\t " +
                $"{Specialty}\t " +
                $"{Progress} \n";
        }
    }
}