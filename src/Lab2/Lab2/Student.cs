﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    class Student
    {
        public string surname { get; set; }
        public string name { get; set; }
        public string patronymic { get; set; }
        public DateTime date { get; set; }
        public DateTime receipt_data { get; set; }
        public string group_index { get; set; }
        public string faculty { get; set; }
        public string specialty { get; set; }
        public int performance { get; set; }

        public Student() { }

        public Student(string surname, string name, string patronymic, DateTime dob, DateTime enterDate, string groupIndex, string faculty, string specialty, int academicPerformance)
        {
            this.surname = surname;
            this.name = name;
            this.patronymic = patronymic;
            date = dob;
            this.receipt_data = enterDate;
            this.group_index = groupIndex;
            this.faculty = faculty;
            this.specialty = specialty;
            this.performance = academicPerformance;
        }

        public override string ToString()
        {
            return $"Surname: {surname}\n" +
                   $"Name: {name}\n" +
                   $"Patronymic: {patronymic}\n" +
                   $"Date of birth: {date.ToString()}\n" +
                   $"Enter date: {receipt_data}\n" +
                   $"Group index: {group_index}\n" +
                   $"Faculty: {faculty}\n" +
                   $"Specialty: {specialty}\n" +
                   $"Academic performance: {performance}\n";
        }
    }
}
