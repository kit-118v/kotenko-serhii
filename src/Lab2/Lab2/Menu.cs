﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    class Menu
    {
        public static void Main()
        {
            var studentArray = new StudentArray();
            while (true)
            {
                PrintMenu();
                int choice;
                try
                {
                    choice = Input.EnterInt("option");
                    switch (choice)
                    {
                        case 1:
                            {
                                studentArray.AddStudent(CreateStudent());
                                break;
                            }
                        case 2:
                            {
                                studentArray.print_students();
                                studentArray.DeleteStudentByIndex(Input.EnterInt("index"));
                                break;
                            }
                        case 3:
                            {
                                studentArray.print_students();
                                break;
                            }
                        case 4:
                            {
                                int searchChoice = Input.EnterInt("index");
                                Console.WriteLine(studentArray.findStudent(searchChoice));
                                break;
                            }
                        case 0:
                            {
                                return;
                            }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static void PrintMenu()
        {
            Console.WriteLine("Enter your option:");
            Console.WriteLine("1. Add student");
            Console.WriteLine("2. Remove student");
            Console.WriteLine("3. Print students");
            Console.WriteLine("4. Search by index");
            Console.WriteLine("0. Exit\n");
        }

        public static Student CreateStudent()
        {
            string surname = Input.EnterName("student's Surname ");
            string name = Input.EnterName("student's Name ");
            string patronymic = Input.EnterName("student's Patronymic ");
            DateTime dob = Input.EnterDate("day of birth ");
            DateTime enterDate = Input.EnterDate("acquiring date ");
            string groupIndex = Input.EnterString("group index ");
            string faculty = Input.EnterUniInfo("faculty ");
            string specialty = Input.EnterUniInfo("speciality ");
            int academicPerformance = Input.EnterPercents("academic performance, in percents ");
            return new Student(surname, name, patronymic, dob, enterDate, groupIndex, faculty, specialty, academicPerformance);
        }
    }
}
