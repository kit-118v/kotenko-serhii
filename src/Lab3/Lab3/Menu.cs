﻿using System;
using System.IO;

namespace Lab3
{
    class Menu
    {
        static void Main(string[] args)
        {
            var studArr = new StudentArray();
            bool flag = true;
            string name, lastname, patronym, faculty, specialty;
            string path = @"C:\Users\Сергей\source\repos\Lab3\Lab3\Lab3_text.txt";
            DateTime birthday, date;
            int progress, number;
            string index;
            string text;
            int choice;
            while (flag)
            {
                Console.WriteLine("Что Вы хотите сделать?\n" +
                    "1 - Добавить данные про студента\n" +
                    "2 - Вывести на экран данные\n" +
                    "3 - Записать данные в файл\n" +
                    "4 - Прочитать данные из файла\n" +
                    "5 - Найти элемент по индексу\n" +
                    "6 - Удалить данные о студенте\n" +
                    "7 - Редактировать данные студента\n" +
                    "8 - Выйти\n");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        name = Input.EnterName("Имя");
                        lastname = Input.EnterName("Фамилия");
                        patronym = Input.EnterName("Отчество");
                        birthday = Input.EnterDate("День рождения");
                        date = Input.EnterDate("Дата поступления");
                        index = Input.EnterUniInfo("Индекс группы");
                        faculty = Input.EnterUniInfo("Факультет");
                        specialty = Input.EnterUniInfo("Специальность");
                        progress = Input.EnterPercents("Успеваемость, в процентах");
                        var stud = new Student(name, lastname, patronym, birthday, date, index, faculty, specialty, progress);
                        studArr.Add(stud);
                        break;
                    case 2:
                        foreach (var student in studArr)
                        {
                            Console.WriteLine(student + " ");
                        }
                        break;
                    case 3:
                        try
                        {
                            using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                            {
                                sw.WriteLine("Студенты: ");
                            }
                            foreach (var student in studArr)
                            {
                                using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
                                {
                                    sw.WriteLine(student);
                                }
                            }
                            Console.WriteLine("Запись выполнена");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 4:
                        try
                        {
                            using (StreamReader sr = new StreamReader(path))
                            {
                                text = sr.ReadToEnd();

                                string[] separatingStrings = { " ", "\r", "\n", ":", "00:00:00", "\t", "Студенты", "Имя", "Фамилия", "Отчество", "День рождения",
                                    "Дата поступления", "Индекс группы", "Факультет", "Специальность", "Успеваемость"};
                                string[] words = text.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);
                                for (int i = 0; i < words.Length / 9; i++)
                                {
                                    name = words[9 * i];
                                    lastname = words[9 * i + 1];
                                    patronym = words[9 * i + 2];
                                    birthday = DateTime.Parse(words[9 * i + 3]);
                                    date = DateTime.Parse(words[9 * i + 4]);
                                    index = words[9 * i + 5];
                                    faculty = words[9 * i + 6];
                                    specialty = words[9 * i + 7];
                                    progress = int.Parse(words[9 * i + 8]);
                                    stud = new Student(name, lastname, patronym, birthday, date, index, faculty, specialty, progress);
                                    studArr.Add(stud);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 5:
                        Console.WriteLine("Номер студента, которого хотите найти: ");
                        number = int.Parse(Console.ReadLine());
                        studArr.Search(studArr, number);
                        break;
                    case 6:
                        Console.WriteLine("Номер студента, данные о котором хотите удалить: ");
                        number = int.Parse(Console.ReadLine());
                        studArr.Remove(studArr, number);
                        break;
                    case 7:
                        Console.WriteLine("Номер студента, данные о котором хотите отредактировать: ");
                        number = int.Parse(Console.ReadLine());
                        Console.WriteLine("Что хотите отредактировать? (1-имя, 2 - фамилию, 3 - отчество, 4 - день рождения, 5 - дата поступления, " +
                                        "6 - индекс группы, 7 - факультет, 8 - специальность, 9 - успеваемость");
                        int n;
                        string str;
                        n = int.Parse(Console.ReadLine());
                        Console.WriteLine("Введите новые данные в соответствующем формате : ");
                        str = Console.ReadLine();
                        switch (n)
                        {
                            case 1:
                                studArr.Edit(studArr, number, "name", str);
                                break;
                            case 2:
                                studArr.Edit(studArr, number, "lastname", str);
                                break;
                            case 3:
                                studArr.Edit(studArr, number, "patronym", str);
                                break;
                            case 4:
                                studArr.Edit(studArr, number, "birthday", str);
                                break;
                            case 5:
                                studArr.Edit(studArr, number, "date", str);
                                break;
                            case 6:
                                studArr.Edit(studArr, number, "index", str);
                                break;
                            case 7:
                                studArr.Edit(studArr, number, "faculty", str);
                                break;
                            case 8:
                                studArr.Edit(studArr, number, "specialty", str);
                                break;
                            case 9:
                                studArr.Edit(studArr, number, "progress", str);
                                break;
                        }
                        break;
                    case 8:
                        flag = false;
                        break;
                }
            }
        }
    }
}
